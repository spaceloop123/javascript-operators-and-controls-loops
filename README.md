# JavaScript Operators and Control Loops

## Tasks

#### Repeat String
Return a string that repeated the specified number of times.
For example:
```js
'A', 5  => 'AAAAA'
'cat', 3 => 'catcatcat'
```
Write your code in `src/index.js` within an appropriate function `repeatString`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Extract emails
Extract e-mails from single string with e-mails list delimeted by semicolons.
For example:
```js
'angus.young@gmail.com;brian.johnson@hotmail.com;bon.scott@yahoo.com' => ['angus.young@gmail.com', 'brian.johnson@hotmail.com', 'bon.scott@yahoo.com']
'info@gmail.com' => ['info@gmail.com']
```
Write your code in `src/index.js` within an appropriate function `extractEmails`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### ROT13
Encode specified string with [ROT13](https://en.wikipedia.org/wiki/ROT13) cipher.
For example:
```js
'hello' => 'uryyb'
'Why did the chicken cross the road?' => 'Jul qvq gur puvpxra pebff gur ebnq?'
'Gb trg gb gur bgure fvqr!' => 'To get to the other side!'
'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' => 'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm'
```
Write your code in `src/index.js` within an appropriate function `encodeToRot13`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Is String
Return true if the value is string; otherwise false.
For example:
```js
'' => true
null => false
[] => false
{} => false
'test' => true
new String('test') => true
```
Write your code in `src/index.js` within an appropriate function `isString`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Last digit
Return a last digit of an integer number.
For example:
```js
100     => 0
37     => 7
5     => 5
0     => 0
```
Write your code in `src/index.js` within an appropriate function `getLastDigit`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Is Prime
Return true is the number is prime; otherwise false.
For example:
```js
4 => false
5 => true
6 => false
7 => true
11 => true
12 => false
16 => false
17 => true
```
Write your code in `src/index.js` within an appropriate function `isPrime`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### To Number
Try to convert value to number and return it if conversion was successful;
otherwise returns default value passed as a second argument.
For example:
```js
toNumber(null, 0) => 0
toNumber('test', 0) => 0
toNumber('1', 0) => 1
toNumber(42, 0) => 42
toNumber(new Number(42), 0) => 42
```
Write your code in `src/index.js` within an appropriate function `toNumber`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)   
2. Fork this repository: javascript-operators-and-controls-loops
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/javascript-operators-and-controls-loops/  
4. Go to folder `javascript-operators-and-controls-loops`  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests you 100% of passing tests is equal to 100p in score  

## Submit to [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/)
1. Open [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/) and login
2. Navigate to the JavaScript Mentoring Course page
3. Select your task (javascript-operators-and-controls-loops)
4. Press the submit button and enjoy, results will be available in few minutes

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.

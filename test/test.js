const assert = require('assert');
const tasks = require('src');

describe('strings-tasks', function() {

    it('repeatString should repeat string specified number of times', function() {
        assert.equal('AAAAA', tasks.repeatString('A', 5));
        assert.equal('catcatcat', tasks.repeatString('cat', 3));
    });

    it('extractEmails should extract emails from string list delimeted by semicolons', function() {
        assert.deepEqual(
            ['angus.young@gmail.com', 'brian.johnson@hotmail.com', 'bon.scott@yahoo.com'],
            tasks.extractEmails('angus.young@gmail.com;brian.johnson@hotmail.com;bon.scott@yahoo.com')
        );
        assert.deepEqual(
            ['info@gmail.com'],
            tasks.extractEmails('info@gmail.com')
        );
    });

    it('encodeToRot13 should encode-decode string using ROT13 algorithm', function() {
        assert.equal('uryyb', tasks.encodeToRot13('hello'));
        assert.equal('Jul qvq gur puvpxra pebff gur ebnq?', tasks.encodeToRot13('Why did the chicken cross the road?'));
        assert.equal('To get to the other side!', tasks.encodeToRot13('Gb trg gb gur bgure fvqr!'));
        assert.equal(
            'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm',
            tasks.encodeToRot13('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
        );
    });

    it('isString should return true if argument ia a string', function() {
        assert.equal(false, tasks.isString(), "undefined");
        assert.equal(false, tasks.isString(null), "null");
        assert.equal(false, tasks.isString([]), "[]");
        assert.equal(true, tasks.isString('test'), "test");
        assert.equal(true, tasks.isString(new String('test')), "new String('test')");
    });
});

describe('numbers-tasks', function() {

    it('getLastDigit should return a last digit of the number', function() {
        assert.equal(0, tasks.getLastDigit(100));
        assert.equal(7, tasks.getLastDigit(37));
        assert.equal(5, tasks.getLastDigit(5));
        assert.equal(0, tasks.getLastDigit(0));
    });

    it('isPrime should return true if specified number is prime', function() {
        assert.equal(true, tasks.isPrime(2), "2");
        assert.equal(true, tasks.isPrime(3), "3");
        assert.equal(false, tasks.isPrime(4), "4");
        assert.equal(true, tasks.isPrime(5), "5");
        assert.equal(false, tasks.isPrime(6), "6");
        assert.equal(true, tasks.isPrime(7), "7");
        assert.equal(false, tasks.isPrime(8), "8");
        assert.equal(false, tasks.isPrime(9), "9");
        assert.equal(false, tasks.isPrime(10), "10");
        assert.equal(true, tasks.isPrime(11), "11");
        assert.equal(false, tasks.isPrime(12), "12");
        assert.equal(true, tasks.isPrime(13), "13");
        assert.equal(true, tasks.isPrime(113), "113");
        assert.equal(false, tasks.isPrime(119), "119");
    });

    it('toNumber should convert any value to number or return the default', function() {
        assert.equal(0, tasks.toNumber(null, 0));
        assert.equal(0, tasks.toNumber('test', 0));
        assert.equal(1, tasks.toNumber('1', 0));
        assert.equal(42, tasks.toNumber(42, 0));
        assert.equal(42, tasks.toNumber(new Number(42), 0));
        assert.equal(-1, tasks.toNumber(undefined, -1));
    });
});
